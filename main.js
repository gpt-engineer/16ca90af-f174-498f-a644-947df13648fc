let startTime, endTime;
let highScore = localStorage.getItem('highScore') || 0;
let attempts = localStorage.getItem('attempts') || 0;

document.getElementById('highScore').innerText = `High Score: ${highScore}ms`;
document.getElementById('attempts').innerText = `Attempts: ${attempts}`;

document.getElementById('clickArea').addEventListener('click', (event) => {
  const clickArea = event.target;
  if (clickArea.classList.contains('bg-red-500')) {
    clickArea.classList.remove('bg-red-500');
    clickArea.classList.add('bg-green-500');
    startTime = new Date();
  } else if (clickArea.classList.contains('bg-green-500')) {
    clickArea.classList.remove('bg-green-500');
    clickArea.classList.add('bg-red-500');
    endTime = new Date();
    const reactionTime = endTime - startTime;
    attempts++;
    if (reactionTime < highScore || highScore === 0) {
      highScore = reactionTime;
      localStorage.setItem('highScore', highScore);
    }
    localStorage.setItem('attempts', attempts);
    document.getElementById('highScore').innerText = `High Score: ${highScore}ms`;
    document.getElementById('attempts').innerText = `Attempts: ${attempts}`;
  }
});
